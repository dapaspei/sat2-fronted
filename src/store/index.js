import { createStore } from "vuex";
import { auth } from "./auth.module";

const store = createStore({
  strict: true,
  state: {
    sideBarSelection: '',
    searchFilter: '',
    challenges: [],
    challengesLoaded: false,
    users: [],
    usersLoaded: false,
    rubrics: [],
    rubricsLoaded: false,
    techSkills: [],
    techSkillsLoaded: false,

  },
  modules: {
    auth,
    //users,
    //rubrics,
    //challenges,
    //technical_skills,

  },
  getters: {
    getSideBarSelection: (state) => state.sideBarSelection,
    getSearchText: (state) => () => state.searchFilter,

    // CHALLENGES
    getChallengesLoaded: (state) => () => state.challengesLoaded,
    getAllChallenges: (state) => () => state.challenges,
    getChallengesCount: (state) => () => state.challenges.length,
    getChallengeById: (state) => (challengeId) => state.challenges.find(ch => ch.id === Number(challengeId)),


    // USERS
    getUsersLoaded: (state) => () => state.usersLoaded,
    getAllUsers: (state) => () => state.users,
    getUsersCount: (state) => () => state.users.length,
    //getAllUsersWithFullName: (state) => state.usersWithFullName,
    getUserWithFullName: (state) => (id) => {
      //console.log("en getUWFN, id=")
      //console.log(id)
      const userFounded = state.usersWithFullName.find(user => user.id === id)
      //console.log(userFounded)
      return userFounded
        //return state.usersWithFullName.find(user => user.id === id) 
      },
    getFullName: (state) => (id) => {
      const userFounded = state.users.find(u => u.id === id)
      if (userFounded)
        return userFounded.fullname
      else
        return ''
    },
    getUserById: (state) => (userId) => state.users.find(u => u.id === Number(userId)),
    /*getUserById: (state) => (userId) => { 
      console.log("en getUById, id=")
      console.log(userId)
      console.log("allUsers")
      console.log(state.users)
      const userFounded = state.users.find(u => u.id === Number(userId))
      console.log(userFounded)
      return userFounded
    },*/
    getAllTeachers: (state) => () => state.users.filter(u => u.role === 'teacher'),
    getAllStudents: (state) => () => state.users.filter(u => u.role === 'student'),

    // RUBRICS
    getRubricsLoaded: (state) => () => state.rubricsLoaded,
    getAllRubrics: (state) => () => state.rubrics,
    getRubricByName: (state) => (rubricName) => state.rubrics.find(r => r.name === rubricName),
    getRubricById: (state) => (rubricId) => state.rubrics.find(r => r.id === Number(rubricId)),

    // TECHNICAL SKILLS
    getTechnicalSkillsLoaded: (state) => () => state.techSkillsLoaded,
    getAllTechnicalSkills: (state) => () => state.techSkills,
    getTechnicalSkillByName: (state) => (techSkillName) => state.techSkills.find(ts => ts.name === techSkillName) ,
    getTechnicalSkillById: (state) => (techSkillId) => state.techSkills.find(ts => ts.id === Number(techSkillId)),
      

  },
  mutations: {
    CHANGE_SIDEBAR_SELECTION: (state, newSelection) => { state.sideBarSelection = newSelection },
    CHANGE_SEARCH_TEXT: (state, newText) => state.searchFilter = newText,

    // CHALLANGES
    CHANGE_CHALLENGES_LOADED: (state, newState) => state.challengesLoaded = newState,
    ADD_CHALLENGES: (state, allChallenges) => state.challenges = allChallenges,
    ADD_NEW_CHALLENGE: (state, newChallenge) => state.challenges.push(newChallenge),
    UPDATE_CHALLENGE: (state, updatedChallenge) =>
      state.challenges[state.challenges.findIndex(ch => ch.id === Number(updatedChallenge.id))] = updatedChallenge,
    DELETE_CHALLENGE: (state, challengeId) => {
      let restChallenges = state.challenges.filter(ch => ch.id != challengeId)
      state.challenges = restChallenges
    },

    // USERS
    CHANGE_USERS_LOADED: (state, newState) => state.usersLoaded = newState,
    ADD_USERS: (state, newAllUsers) => state.users =  newAllUsers,
    ADD_NEW_USER: (state, newUser) => state.users.push(newUser),
    //ADD_USER_WITH_FULL_NAME: (state, newUser) => state.usersWithFullName.push(newUser),
    UPDATE_USER: (state, updatedUser) => state.users[state.users.findIndex(u => u.id === Number(updatedUser.id))] = updatedUser,
    DELETE_USER: (state, userId) => {
      let restUsers = state.users.filter(u => u.id !== userId)
      state.users = restUsers;
      let restUserFullNames = state.usersWithFullName.filter(u => u.id !== userId)
      state.usersWithFullName = restUserFullNames
    },

    // RUBRICS
    CHANGE_RUBRICS_LOADED: (state, newState) => state.rubricsLoaded = newState,
    ADD_RUBRICS: (state, newRubrics) => state.rubrics = newRubrics,
    ADD_NEW_RUBRIC: (state, newRubric) => state.rubrics.push(newRubric),
    UPDATE_RUBRIC: (state, updatedRubric) =>
      state.rubrics[state.rubrics.findIndex(r => r.id === Number(updatedRubric.id))] = updatedRubric,
    DELETE_RUBRIC: (state, rubricId) => {
      let restRubrics = state.rubrics.filter(r => r.id !== rubricId)
      state.rubrics = restRubrics
    },


    // TECHNICAL SKILLS
    CHANGE_TECHSKILLS_LOADED: (state, newState) => state.techSkillsLoaded = newState,
    ADD_TECHSKILLS: (state, allTechSkills) => state.techSkills = allTechSkills,
    ADD_NEW_TECHSKILL: (state, newTechSkill) => state.techSkills.push(newTechSkill),
    UPDATE_TECHSKILL: (state, updatedTechSkill) =>
      state.techSkills[state.techSkills.findIndex(ts => ts.id === Number(updatedTechSkill.id))] = updatedTechSkill,
      //Object.assign(state.techSkills[state.techSkills.findIndex(ts => ts.id === updatedTechSkill.id)], updatedTechSkill),
      //state.techSkills[state.techSkills.findIndex(ts => ts.id === updatedTechSkill.id)] = Object.assign({}, updatedTechSkill),*/
    /*UPDATE_TECHSKILL: (state, updatedTechSkill) => {
      let index = state.techSkills.findIndex(ts => ts.id === Number(updatedTechSkill.id))
      console.log("ts id = " + index)
      console.log("updatedTS:")
      console.log(updatedTechSkill)
      console.log("actual TS:")
      console.log(state.techSkills[index])
      state.techSkills[index] = updatedTechSkill
      console.log("AFTER Modified in VUEX")
      console.log(state.techSkills[index])

      //state.techSkills[state.techSkills.findIndex(ts => ts.id === updatedTechSkill.id)] = updatedTechSkill,

    },*/
    DELETE_TECHSKILL: (state, techSkillId) => {
      let restTechSkills = state.techSkills.filter(ts => ts.id !== techSkillId)
      state.techSkills = restTechSkills
    }
  },
});

export default store;
