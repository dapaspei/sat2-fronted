import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'https://ci4.danielpastor.pro/';

class RubricService {

  async getRubricList() {
    return await axios.get(API_URL + 'rubrics', {
      headers: authHeader()
    })
  }
  async getUserById(id) {
    const rubricid = `rubrics/${id}`
    return await axios.get(API_URL + rubricid, {
      headers: authHeader()
    })
  }

  // RUBRIC CRUD
  async create(rubric) {
    //console.log(rubric)
    
    return await axios.post(API_URL + 'rubrics', {
      name: rubric.name,
      description: rubric.description,
      cols: rubric.cols,
      rows: rubric.rows,
      min_value: rubric.min_value,
      max_value: rubric.max_value,
      data: rubric.data,
      t_owner: rubric.t_owner,
      t_available_for: rubric.t_available_for,
      teaching_levels: rubric.teaching_levels,
    }, {
      headers: authHeader()
    })
  }

  async update(id, rubric) {
    let headersData = authHeader()
    //headersData['Content-Type'] = 'application/x-www-form-urlencoded'
    //headersData['Content-Type'] = 'application/json'
    //headersData['X-Requested-With']='XMLHttpRequest'

    //console.log("headersDAta:")
    //console.log(headersData)

    // axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

    /*const request = new FormData()
    request.append('id',id)
    request.append('name', rubric.name)
    request.append('description', rubric.description)
    request.append('cols', rubric.cols)
    request.append('rows', rubric.rows)
    request.append('min_value', rubric.min_value)
    request.append('max_value', rubric.max_value)
    request.append('data', rubric.data)
    request.append('t_owner', rubric.t_owner)
    request.append('t_available_for', rubric.t_available_for)
    request.append('teaching_levels', rubric.teaching_levels)*/
    
    try {
      return await axios.put(API_URL + `rubrics/${id}`,
        //request,
        {
        id: id,
        name: rubric.name,
        description: rubric.description,
        cols: rubric.cols,
        rows: rubric.rows,
        min_value: rubric.min_value,
        max_value: rubric.max_value,
        data: rubric.data,
        t_owner: rubric.t_owner,
        t_available_for: rubric.t_available_for,
        teaching_levels: rubric.teaching_levels,
        //_method: 'patch',
      },
        { headers: headersData }
        //{ headers: authHeader() }

      );
    } catch (error) {
      if (error.response) {
        // Request made and server responded
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      } else if (error.request) {
        // The request was made but no response was received
        console.log(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
      }
    }
  }

  async delete(id) {
    /*console.log("before delete, url =")
    let url = API_URL + `rubrics/${id}`
    console.log(url)
    console.log("-----");*/
    return await axios.delete(API_URL + `rubrics/${id}`, {
      headers: authHeader()
    })
  }
}

export default new RubricService();
