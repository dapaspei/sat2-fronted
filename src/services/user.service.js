import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'https://ci4.danielpastor.pro/';

class UserService {

  async getUsersList() {
    //console.log("getUserList, authheader:");
    //console.log(authHeader());
    return await axios.get(API_URL + 'users', {
      headers: authHeader()
    })
  }
  async getUserById(id) {
    const userid = `users/${id}`
    return await axios.get(API_URL + userid, {
      headers: authHeader()
    })
  }

  // USER CRUD
  async create(user) {
    console.log(user)
    
    return await axios.post(API_URL + 'users', {
      username: user.username,
      firstname: user.firstname,
      lastname: user.lastname,
      email: user.email,
      password: user.password,
      role: user.role,
      t_coordinator: user.t_coordinator,
      teaching_levels: user.teaching_levels,
    }, {
      headers: authHeader()
    })
  } 

  async update(id, user) {
    //let headersData = authHeader()
    //headersData['Content-Type'] = 'application/x-www-form-urlencoded'
    //headersData['Content-Type'] = 'application/json'

    //console.log("headersDAta:")
    //console.log(headersData)

    return await axios.put(API_URL + `users/${id}`, {
      username: user.username,
      firstname: user.firstname,
      lastname: user.lastname,
      email: user.email,
      password: user.password,
      role: user.role,
      t_coordinator: user.t_coordinator,
      teaching_levels: user.teaching_levels,
    },
      //{ headers: headersData }
    {
      headers: authHeader()
    })
  }

  async delete(id) {
    /*console.log("before delete, url =")
    let url = API_URL + `users/${id}`
    console.log(url)
    console.log("-----");*/
    return await axios.delete(API_URL + `users/${id}`, {
      headers: authHeader()
    })
  }
}

export default new UserService();
