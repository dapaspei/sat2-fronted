import axios from 'axios';

const API_URL = 'https://ci4.danielpastor.pro/auth/';

class AuthService {
  async login(user) {
    const response = await axios
      .post(API_URL + 'signin', {
        username: user.username,
        password: user.password
      });
    
    let responseData = response.data

    if (responseData.access_token) {
      console.log("login -> response.data="+JSON.stringify(responseData))
      localStorage.setItem('user', JSON.stringify(responseData));
      //localStorage.setItem('user', JSON.stringify(response.data.user))
      //localStorage.setItem('access_token', JSON.stringify(response.data.access_token))
    }
    
    //if (response.data.accessToken) {
    /*if (response.data.access_token) {
      console.log("login -> response.data.access_token="+JSON.stringify(response.data.access_token))
      //localStorage.setItem('user', JSON.stringify(response.data));
      localStorage.setItem('user', JSON.stringify(response.data.user))
      localStorage.setItem('access_token', JSON.stringify(response.data.access_token))
    }
    console.log("return response.data="+JSON.stringify(response.data));
    //return response.data;*/
    return responseData.user;
  }

  logout() {
    localStorage.removeItem('user')
    localStorage.removeItem('access_token')
  }

  register(user) {
    //console.log(user)
    //return true;
    return axios.post(API_URL + 'signup', {
      username: user.username,
      email: user.email,
      password: user.password,
      role: user.role,
    });
  }

  /*async delete(userId) {
    console.log("userID="+userId)
    return axios.delete(API_URL + `delete/${userId}`);
  }*/
}

export default new AuthService();
