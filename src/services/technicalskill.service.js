import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'https://ci4.danielpastor.pro/technical-skills';

class TechnicalSkillService {

  async getTechnicalSkillList() {
    return await axios.get(API_URL + '', {
      headers: authHeader()
    })
  }
  async getTechnialSkillById(id) {
    const techSkillId = `/${id}`
    return await axios.get(API_URL + techSkillId, {
      headers: authHeader()
    })
  }

  // techSkill CRUD
  async create(techSkill) {
    //console.log(techSkill)
    
    return await axios.post(API_URL + '', {
      name: techSkill.name,
      description: techSkill.description,
      school_year: techSkill.school_year,
      has_rubric: techSkill.has_rubric,
      rubric_id: techSkill.rubric_id,
      //challenges_in: JSON.stringify(techSkill.challenges_in),
      challenges_in: techSkill.challenges_in.join(), // Convertir array en String
      t_owner: techSkill.t_owner,
    }, {
      headers: authHeader()
    })
  }

  async update(id, techSkill) {
    let headersData = authHeader()
  
    try {
      return await axios.put(API_URL + `/${id}`,
        //request,
        {
          id: id,
          name: techSkill.name,
          description: techSkill.description,
          school_year: techSkill.school_year,
          has_rubric: techSkill.has_rubric,
          rubric_id: techSkill.rubric_id,
          //challenges_in: JSON.stringify(techSkill.challenges_in),
          challenges_in: techSkill.challenges_in.join(), // Convertir array en String
          t_owner: techSkill.t_owner,
        },
        { headers: headersData }
        //{ headers: authHeader() }

        );
    } catch (error) {
      if (error.response) {
        // Request made and server responded
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      } else if (error.request) {
        // The request was made but no response was received
        console.log(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
      }
    }
  }

  async delete(id) {

    return await axios.delete(API_URL + `/${id}`, {
      headers: authHeader()
    })
  }
}

export default new TechnicalSkillService();
