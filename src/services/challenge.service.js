import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'https://ci4.danielpastor.pro/challenges';

class ChallengeService {

  async getChallengeList() {
    return await axios.get(API_URL + '', {
      headers: authHeader()
    })
  }
  async getChallengeById(id) {
    const challengeId = `/${id}`
    return await axios.get(API_URL + challengeId, {
      headers: authHeader()
    })
  }

  // challenge CRUD
  async create(challenge) {
    //console.log(challenge)
    
    return await axios.post(API_URL + '', {
      name: challenge.name,
      description: challenge.description,
      year: challenge.year,
      start_date: challenge.start_date,
      finish_date: challenge.finish_date,
      sprints: challenge.sprints,
      sprints_openfor_assess: challenge.sprints_openfor_assess,
      teams: challenge.teams,
      t_owner: challenge.t_owner,
      teaching_levels: challenge.teaching_levels,
    }, {
      headers: authHeader()
    })
  }

  async update(id, challenge) {
    let headersData = authHeader()
    //headersData['Content-Type'] = 'application/x-www-form-urlencoded'
    //headersData['Content-Type'] = 'application/json'
    //headersData['X-Requested-With']='XMLHttpRequest'

    //console.log("headersDAta:")
    //console.log(headersData)

    // axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

    
    try {
      return await axios.put(API_URL + `/${id}`,
        //request,
        {
          id: id,
          name: challenge.name,
          description: challenge.description,
          year: challenge.year,
          start_date: challenge.start_date,
          finish_date: challenge.finish_date,
          sprints: challenge.sprints,
          //sprints_openfor_assess: challenge.sprints_openfor_assess,
          sprints_date: challenge.sprints_date,
          teams: challenge.teams,
          subject: challenge.subject,
          soft_percentage: challenge.soft_percentage,
          hard_percentage: challenge.hard_percentage,
          rubric_id: challenge.rubric_id,
          technical_skills: challenge.technical_skills,
          t_owner: challenge.t_owner,
          teaching_levels: challenge.teaching_levels,
        //_method: 'patch',
      },
        { headers: headersData }
        //{ headers: authHeader() }

      );
    } catch (error) {
      if (error.response) {
        // Request made and server responded
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      } else if (error.request) {
        // The request was made but no response was received
        console.log(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
      }
    }
  }

  async delete(id) {
    /*console.log("before delete, url =")
    let url = API_URL + `challenges/${id}`
    console.log(url)
    console.log("-----");*/
    return await axios.delete(API_URL + `/${id}`, {
      headers: authHeader()
    })
  }
}

export default new ChallengeService();
