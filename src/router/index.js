import { createWebHistory, createRouter } from 'vue-router'
//import Home from '@/components/Home.vue'
import Dashboard from '@/Views/DashboardLayout'
import Login from '@/components/Login'
import Logout from '@/components/Logout'
// lazy-loaded
const Register = () => import(/* webpackChunkName: "chunks/register" */ '@/components/Register')
const Profile = () => import(/* webpackChunkName: "chunks/profile" */ '@/components/Profile')
//const AdminBoard = () => import(/* webpackChunkName: "chunks/admin-board" */ '@/components/AdminBoard.vue')
//const TeacherBoard = () => import(/* webpackChunkName: "chunks/teacher-board" */ '@/components/TeacherBoard.vue')
//const StudentBoard = () => import(/* webpackChunkName: "chunks/student-board" */ '@/components/StudentBoard.vue')
const ChallengeList = () => import(/* webpackChunkName: "chunks/challenge-list" */ '@/components/ChallengeList')
const ChallengeAdd = () => import(/* webpackChunkName: "chunks/challenge-add" */ '@/components/ChallengeAdd')
const ChallengeEdit = () => import(/* webpackChunkName: "chunks/challenge-edit" */ '@/components/ChallengeEdit')
const TechnicalSkillList = () => import(/* webpackChunkName: "chunks/technical-skills-list" */ '@/components/TechnicalSkillList')
const TechnicalSkillAdd = () => import(/* webpackChunkName: "chunks/technical-skills-add" */ '@/components/TechnicalSkillAdd')
const TechnicalSkillEdit = () => import(/* webpackChunkName: "chunks/technical-skills-edit" */ '@/components/TechnicalSkillEdit')
const UserList = () => import(/* webpackChunkName: "chunks/user-list" */ '@/components/UserList')
const UserAdd = () => import(/* webpackChunkName: "chunks/user-add" */ '@/components/UserAdd')
const UserEdit = () => import(/* webpackChunkName: "chunks/user-edit" */ '@/components/UserEdit')
const RubricList = () => import(/* webpackChunkName: "chunks/rubric-list" */ '@/components/RubricList')
const RubricAdd = () => import(/* webpackChunkName: "chunks/rubric-add" */ '@/components/RubricAdd')
const RubricEdit = () => import(/* webpackChunkName: "chunks/rubric-edit" */ '@/components/RubricEdit')


const routes = [
  // https://stackoverflow.com/questions/46456233/vue-router-setup-for-landing-login-page-and-then-backend-with-navigation
  {
    name: 'Dashboard',
    path: '/',
    redirect: 'challenges',
    component: Dashboard,
    children: [
      /*{
        path: '',
        name: 'DashboadInit',
        component: DashboardInit,
      },*/
      {
        name: "Challenges",
        path: "challenges",
        component: ChallengeList,
      },
      {
        name: "ChallengeAdd",
        path: "challenge-add",
        component: ChallengeAdd,
      },
      {
        name: 'ChallengeEdit',
        path: 'challenge-edit/:challengeId',
        component: ChallengeEdit,
        props: true,
      },
      {
        name: 'Users',
        path: 'users',
        component: UserList,
        beforeEnter: (_to, _from, next) => {
          const loggedIn = JSON.parse(localStorage.getItem('user'))
          if(loggedIn.user.role !== 'admin')
            // reject the navigation and redirect
            next('/rubrics')
          else
            next()
        },
      },
      {
        name: 'UserAdd',
        path: 'user-add',
        component: UserAdd,
      },
      {
        name: 'UserEdit',
        path: 'user-edit/:userId',
        component: UserEdit,
        props: true
      },
      {
        name: "TechSkills",
        path: "technical-skills",
        component: TechnicalSkillList,
      },
      {
        name: "TechSkillsAdd",
        path: "technical-skills-add",
        component: TechnicalSkillAdd,
      },
      {
        name: 'TechSkillsEdit',
        path: 'technical-skills-edit/:technicalId',
        component: TechnicalSkillEdit,
        props: true,
      },
      {
        name: "Rubrics",
        path: "rubrics",
        component: RubricList,
      },
      {
        name: "RubricAdd",
        path: "rubric-add",
        component: RubricAdd,
      },
      {
        name: 'RubricEdit',
        path: 'rubric-edit/:rubricId',
        component: RubricEdit,
        props: true,
      },
      
      {
        path: "profile",
        name: "Profile",
        component: Profile,
      },
     
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: "/register",
    name: "Register",
    component: Register,
  },
  {
    path: "/logout",
    name: "Logout",
    component: Logout,
  },
 

];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

// eslint-disable-next-line
router.beforeEach((to, from, next) => {
  const publicPages = ['/login', '/register'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');

  // trying to access a restricted page + not logged in
  // redirect to login page
  if (authRequired && !loggedIn) {
    next('/login');
  } else {
    next();
  }
});

export default router;
