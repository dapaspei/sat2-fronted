import { createApp } from "vue"
import App from "./App.vue"
import router from "./router"
import store from "./store"

// styles
import "./assets/tailwind.css"
import { FontAwesomeIcon } from './plugins/font-awesome'

// internacionalización
import { createI18n } from 'vue-i18n'
import { messages } from './plugins/i18nMessages' // Mensajes en 3 idiomas.

//import DashboardLayout from './components/DashboardLayout'


// Global constants
import {
  level_codes,
  level_names,
  school_year,
  sprints_quantity,
  teams_num,
  sprints_min,
  sprints_max,
  soft_skills_percentatge,
  r_min_cols,
  r_max_cols,
  r_min_rows,
  r_max_rows,
  r_min_value_1,
  r_min_value_2,
  r_max_value_1,
  r_max_value_2, } from './plugins/globals'

const i18n = createI18n({
  locale: 'ca',         // set locale
  fallbackLocale: 'es', // set fallback locale
  messages,             // all languages messages
})

const app = createApp(App)
app.use(router)
app.use(store)
app.use(i18n)
app.component("font-awesome-icon", FontAwesomeIcon)
//app.component('default-layout', DashboardLayout)
app.mount("#app")

app.config.globalProperties.level_codes = level_codes
app.config.globalProperties.level_names = level_names
app.config.globalProperties.school_year = school_year
app.config.globalProperties.sprints_quantity = sprints_quantity
app.config.globalProperties.teams_num = teams_num
app.config.globalProperties.sprints_min = sprints_min
app.config.globalProperties.sprints_max = sprints_max
app.config.globalProperties.soft_skills_percentatge = soft_skills_percentatge
app.config.globalProperties.r_min_cols = r_min_cols
app.config.globalProperties.r_max_cols = r_max_cols
app.config.globalProperties.r_min_rows = r_min_rows
app.config.globalProperties.r_max_rows = r_max_rows
app.config.globalProperties.r_min_value_1 = r_min_value_1
app.config.globalProperties.r_min_value_2 = r_min_value_2
app.config.globalProperties.r_max_var_max_value_1 =r_max_value_1
app.config.globalProperties.r_max_value_2 = r_max_value_2
